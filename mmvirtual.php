<?php
header("content-type: text/json;charset=utf-8");
require_once './bootstrap.inc';
require_once './vendor/autoload.php';

function get_duration($duration) {
  $minutes = ceil($duration / 1000 / 60);
  $minutes += 15 - ($minutes % 15);

  return (int) $minutes;
}

function cmp_duration($a, $b) {
  if ($a['duration'] == $b['duration']) {
    return 0;
  }

  return $a['duration'] > $b['duration'] ? -1 : 1;
}

// 1. Load data from the GET.
$workspace = $_GET['workspace'];
$start     = $end = $_GET['date'];

$report_client = \AJT\Toggl\ReportsClient::factory(array(
  'api_key' => TOGGL_API_TOKEN,
  'debug'   => TOGGL_DEBUG
));
$command       = $report_client->getCommand('Details', array(
  'user_agent'   => 'api_test',
  'workspace_id' => (int) $workspace,
  'since'        => $start,
  'until'        => $end
));
$command->prepare();

$json_data = array();
$data      = $command->execute();
foreach ($data['data'] as $entry) {
  $project = $entry['project'];

  $intranet_id = NULL;
  if (preg_match('/\[id\:[0-9]+\]/', $project, $matches)) {
    $intranet_id = substr($matches[0], 4, -1);
  }

  $json_data[] = array(
    'intranet_id' => $intranet_id,
    'description' => $entry['description'],
    'duration'    => get_duration($entry['dur'])
  );
}

usort($json_data, 'cmp_duration');

echo json_encode($json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
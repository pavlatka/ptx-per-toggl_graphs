<?php

$duration = 380000;

function get_duration($duration) {
  $minutes = ceil($duration / 1000 / 60);

  $minutes += 15 - ($minutes % 15);

  echo $minutes;
}

// 1 hour 12 minutes
$duration = 1000 * 60 * 76;
get_duration($duration);

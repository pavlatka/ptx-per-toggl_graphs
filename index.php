<?php
header("content-type: text/plain;charset=utf-8");
require_once './bootstrap.inc';
require_once './vendor/autoload.php';

use AJT\Toggl\TogglClient;

function get_duration($duration) {
  $minutes = ceil($duration / 1000 / 60);
  $minutes += 15 - ($minutes % 15);

  return (int) $minutes;
}

function get_price($project_name) {
  if (!preg_match('/\[price:[0-9]+\]/', $project_name, $matches)) {
    return 0;
  }

  return trim(substr($matches[0], 7, -1));
}

function count_earning($duration, $per_hour) {
  $hours = $duration / 60;
  $earning = (int)$hours * $per_hour;

  $min = $duration % 60;
  switch($min) {
    case 15:
      $earning += round($per_hour / 4 * 2) / 2;
      break;
    case 30:
      $earning += round($per_hour / 2 * 2) / 2;
      break;
    case 45:
      $earning += round($per_hour / 4 * 3 * 2) / 2;
      break;
  }

  return $earning;
}

function cmp_duration($a, $b) {
  if ($a['duration'] == $b['duration']) {
    return 0;
  }

  return $a['duration'] > $b['duration'] ? -1 : 1;
}

function show_human_time($spent) {
  $hours   = floor($spent / 60);
  $minutes = $spent % 60;

  return sprintf('%02d h %02d m', $hours, $minutes);
}

// if you want to see what is happening, add debug => true to the factory call
$toggl_client = TogglClient::factory(array(
  'api_key' => TOGGL_API_TOKEN,
  'debug'   => TOGGL_DEBUG
));

//Retrieve the Command from Guzzle
$command = $toggl_client->getCommand('GetWorkspaces', array());
$command->prepare();

$data       = $command->execute();
$workspaces = array();
foreach ($data as $workspace) {
  $workspaces[$workspace['id']] = $workspace['name'];
}

$workspace = isset($_GET['workspace']) ? $_GET['workspace'] : array_keys($workspaces)[0];

$start = $end = NULL;
if (isset($_GET['date'])) {
  switch ($_GET['date']) {
    case 'today':
      $start = $end = date('Y-m-d');
      break;
    case 'yesterday':
      $start = $end = date('Y-m-d', strtotime('-1 day'));
      break;
    case 'Monday':
    case 'Tuesday':
    case 'Wednesday':
    case 'Thursday':
    case 'Friday':
    case 'Saturday':
    case 'Sunday':
      $start = $end = date('Y-m-d', strtotime('last ' . $_GET['date']));
      break;
    case 'last_week':
      $start = date('Y-m-d', strtotime('last Monday'));
      $end   = date('Y-m-d', strtotime($start . '+ 6 days'));
      break;
    case 'week':
      $start      = date('Y-m-d');
      $day_number = date('N');
      if ($day_number == 0) {
        $start = date('Y-m-d', strtotime('-6 days'));
      }
      elseif ($day_number != 1) { // Monday
        $start = date('Y-m-d', strtotime((1 - $day_number) . ' days'));
      }
      $end = date('Y-m-d', strtotime($start . '+ 6 days'));
      break;
    case 'month':
      $start = date('Y-m-01');
      list($year, $month) = explode('-', $start);
      $last_day = cal_days_in_month(CAL_GREGORIAN, (int) $month, $year);
      $end      = implode('-', array($year, $month, $last_day));
      break;
    case 'last_month':
      $start = date('Y-m-01', strtotime('-1 month'));
      list($year, $month) = explode('-', $start);

      $last_day = cal_days_in_month(CAL_GREGORIAN, (int) $month, $year);
      $end      = implode('-', array($year, $month, $last_day));
      break;
    case '2_month':
      $start = date('Y-m-01', strtotime('-2 month'));
      list($year, $month) = explode('-', $start);

      $last_day = cal_days_in_month(CAL_GREGORIAN, (int) $month, $year);
      $end      = implode('-', array($year, $month, $last_day));
      break;
  }
}

$report_client = \AJT\Toggl\ReportsClient::factory(array(
  'api_key' => TOGGL_API_TOKEN,
  'debug'   => TOGGL_DEBUG
));
$command       = $report_client->getCommand('Details', array(
  'user_agent'   => 'api_test',
  'workspace_id' => (int) $workspace,
  'since'        => $start,
  'until'        => $end
));
$command->prepare();

$data = $command->execute();

$total     = 0;
$earning   = 0;
$structure = array();
foreach ($data['data'] as $record) {
  $project_id = $record['pid'];
  if (!isset($structure[$project_id])) {
    $structure[$project_id] = array(
      'project'  => $record['project'],
      'per_hour' => get_price($record['project']),
      'duration' => 0,
      'entries'  => array()
    );
  }

  $duration = get_duration($record['dur']);
  $total += $duration;
  $structure[$project_id]['duration'] += $duration;
  $structure[$project_id]['entries'][] = array(
    'duration'    => $duration,
    'description' => $record['description'],
    'date'        => substr($record['start'], 0, 10)
  );
}

// Sort the results.
foreach ($structure as $key => $data) {
  usort($data['entries'], 'cmp_duration');
  $structure[$key] = $data;
}
usort($structure, 'cmp_duration');

// Count earnings.
foreach($structure as $project_id => $project_data) {
  $partial_earning = count_earning($project_data['duration'], $project_data['per_hour']);

  $structure[$project_id]['earning'] = $partial_earning;
  $earning += $partial_earning;
}

// Count whole total.

echo "WORKSPACES \r\n---------\r\n";
foreach ($workspaces as $id => $name) {
  echo "{$id} - {$name} \r\n";
}

$end_human   = date('d/m/Y', strtotime($end));
$start_human = date('d/m/Y', strtotime($start));
$total_human = show_human_time($total);
echo "\r\nOVERVIEW: {$start_human} - {$end_human} | Total: {$total_human} - {$earning} EUR\r\n------------------------------------------------------------------\r\n";
foreach ($structure as $pid => $project) {
  echo $project['project'] . ' - ' . show_human_time($project['duration']) . ' - ' . $project['earning'] . " EUR\r\n";

  foreach ($project['entries'] as $entry) {
    echo '- ' . show_human_time($entry['duration']) . ' - ' . $entry['description'] . "\r\n";
  }

  echo "\r\n";
}

<?php
header("content-type: text/plain;charset=utf-8");
require_once './bootstrap.inc';
require_once './vendor/autoload.php';

use AJT\Toggl\TogglClient;

function get_duration($duration) {
  $minutes = ceil($duration / 1000 / 60);
  $minutes += 15 - ($minutes % 15);

  return (int) $minutes;
}

function cmp_duration($a, $b) {
  if ($a['duration'] == $b['duration']) {
    return 0;
  }

  return $a['duration'] > $b['duration'] ? -1 : 1;
}

function show_human_time($spent) {
  $hours   = floor($spent / 60);
  $minutes = $spent % 60;

  return sprintf('%02d h %02d m', $hours, $minutes);
}

// if you want to see what is happening, add debug => true to the factory call
$toggl_client = TogglClient::factory(array(
  'api_key' => TOGGL_API_TOKEN,
  'debug'   => TOGGL_DEBUG
));

$workspace = 2101981;
$start = $end = NULL;
if (isset($_GET['date'])) {
  switch ($_GET['date']) {
    case 'today':
      $start = $end = date('Y-m-d');
      break;
    case 'yesterday':
      $start = $end = date('Y-m-d', strtotime('-1 day'));
      break;
    case 'Monday':
    case 'Tuesday':
    case 'Wednesday':
    case 'Thursday':
    case 'Friday':
    case 'Saturday':
    case 'Sunday':
      $start = $end = date('Y-m-d', strtotime('last ' . $_GET['date']));
      break;
    case 'last_week':
      $start = date('Y-m-d', strtotime('last Monday -1 week'));
      $end   = date('Y-m-d', strtotime($start . '+ 6 days'));
      break;
    case 'week':
      $start      = date('Y-m-d');
      $day_number = date('N');
      if ($day_number == 0) {
        $start = date('Y-m-d', strtotime('-6 days'));
      }
      elseif ($day_number != 1) { // Monday
        $start = date('Y-m-d', strtotime((1 - $day_number) . ' days'));
      }
      $end = date('Y-m-d', strtotime($start . '+ 6 days'));
      break;
    case 'month':
      $start = date('Y-m-01');
      list($year, $month) = explode('-', $start);
      $last_day = cal_days_in_month(CAL_GREGORIAN, (int) $month, $year);
      $end      = implode('-', array($year, $month, $last_day));
      break;
    case 'last_month':
      $start = date('Y-m-01', strtotime('-1 month'));
      list($year, $month) = explode('-', $start);

      $last_day = cal_days_in_month(CAL_GREGORIAN, (int) $month, $year);
      $end      = implode('-', array($year, $month, $last_day));
      break;
  }
}

$report_client = \AJT\Toggl\ReportsClient::factory(array(
  'api_key' => TOGGL_API_TOKEN,
  'debug'   => TOGGL_DEBUG
));
$command       = $report_client->getCommand('Details', array(
  'user_agent'   => 'api_test',
  'workspace_id' => (int) $workspace,
  'since'        => $start,
  'until'        => $end
));
$command->prepare();

$data = $command->execute();

$total     = 0;
$structure = [];
while ($start <= $end) {
  $structure[$start] = [];

  $start = date('Y-m-d', strtotime($start . '+1 day'));
}

foreach ($data['data'] as $record) {
  $date = substr($record['start'], 0, 10);
  $project_id = $record['pid'];
  $project_name = $record['project'];
  $project_description = $record['description'];
  if (preg_match('/^[0-9]+ +\|/', $project_description)) {
    list($project_id, $project_name) = explode('|', $project_description);
    $project_id = trim($project_id);
    $project_name = trim($project_name);
  }

  if (!isset($structure[$date][$project_id])) {
    $structure[$date][$project_id] = [
      'pid' => $project_id,
      'project' => $project_name,
      'duration' => 0,
      'entries' => []
    ];
  }

  $duration = get_duration($record['dur']);
  $total += $duration;
  $structure[$date][$project_id]['duration'] += $duration;
  $structure[$date][$project_id]['entries'][] = array(
    'duration'    => $duration,
    'description' => $record['description'],
    'date'        => substr($record['start'], 0, 10)
  );
}

$end_human   = date('d/m/Y', strtotime($end));
$start_human = date('d/m/Y', strtotime($start));
$total_human = show_human_time($total);
echo "========================================================\r\n";
echo "| OVERVIEW: {$start_human} - {$end_human} | Total: {$total_human} |\r\n";
echo "========================================================\r\n\r\n";
foreach ($structure as $date => $projects) {
  echo "------------------\r\n";
  echo "| " . date('D', strtotime($date)) . ' ' . date('d.m.Y', strtotime($date)) . " |\r\n";
  echo "------------------\r\n\r\n";
  foreach ($projects as $pid => $project) {
    echo $project['pid'] . ' - ' . $project['project'] . ' - ' . show_human_time($project['duration']) . "\r\n";

    foreach ($project['entries'] as $entry) {
      echo '- ' . show_human_time($entry['duration']) . ' - ' . $entry['description'] . "\r\n";
    }

    echo "\r\n";
  }
}
